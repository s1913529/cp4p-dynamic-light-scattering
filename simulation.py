'''
Simulating the ideal autocorrelation function from a DLS instrument for a given hydrodynamic radius. 


Anshika Binjrajka 
'''

import matplotlib.pyplot as plt
import argparse
import numpy as np
from scipy.constants import k 
from scipy.signal import correlate
from scipy.optimize import curve_fit

def g(tau, a, b, c):
    return a*np.ones_like(tau) + b*np.exp(-c*tau)

def main():
    parser = argparse.ArgumentParser(description='Simulates the autocorrelation of DLS data.')
    parser.add_argument('radius', type=str, help='Radius of the beads in nm')
    parser.add_argument('config', type=str, help='Configuration file for calculation')
    args = parser.parse_args()
    args = vars(args)

    T, eta_s, n_s, theta, lambd = np.loadtxt(args['config'], comments='#')
    theta = np.deg2rad(theta)
    q = 4*np.pi/lambd*n_s*np.sin(theta/2) #Scattering Vector 

    r_h = int(args['radius'])
    r_h = r_h*(10**(-9))

    diff = (k*T)/(6*np.pi*r_h*eta_s)

    alpha = 2*q*q*diff

    time = np.linspace(0,2,10000)
    data = np.exp(-1*alpha*time)

    data2 = np.sin(2*np.pi*time*50)
    noise = np.random.uniform(low=0.95, high=1.05, size=len(time))
    data2 = data2*noise

    func = correlate(data2,data2, mode="same")

    a0 = func[-1]
    b0 = func[0]- a0
    c0 = 2*q**2*k*T/(18*1e-7*eta_s)

    tau = 1e-6*time
    params, error = curve_fit(g, tau, func, p0=[a0,b0,c0])


    label = "Decay Constant:" +str(1/alpha)
    plt.plot(time, data)
    plt.ylabel("")
    plt.title(label)
    plt.xlabel("Time (s)")
    plt.show()

if __name__ == "__main__":
    main()


