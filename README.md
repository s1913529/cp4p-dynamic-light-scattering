# CP4P Dynamic Light Scattering

This repo contains necessary software for both the electronics for data acquisition as well as subsequent data analysis.


# Usage

To run, invoke command

    python3 analyse.py data config outfile [--visualise]


data: input csv, two columns, time and intensity, no heading

config: see example config.txt in this repository

outfile: outfile name, will produce outfile_plot.png, outfile_fig.png

optional flag: --visualise, will produce matplotlib popups
