"""
DLS Analysis Tool

A CLI tool which calculates the autocorrelation
of a time series signal from a DLS setup.

Patrick Wang
s1913529@ed.ac.uk
"""

import numpy as np
import matplotlib.pyplot as plt
import argparse
from scipy.optimize import curve_fit
from scipy.constants import k

def autocorrelate(data):
    N = len(data)
    data_tilde = np.fft.rfft(data)
    return np.fft.irfft(data_tilde*np.conj(data_tilde), N)

def g(tau, a, b, c):
    return a*np.ones_like(tau) + b*np.exp(-c*tau)

def main():
    parser = argparse.ArgumentParser(description='Computes the autocorrelation of DLS data and performs a curve fit.')
    parser.add_argument('data', type=str, help='Two column csv containing time and intensity')
    parser.add_argument('config', type=str, help='Configuration file for calculation')
    parser.add_argument('outfile', type=str, help='File name to write analysis results to')
    parser.add_argument('--visualise', action='store_true', default=False, help ='Enable matplotlib popups.')
    args = parser.parse_args()
    args = vars(args)
    
    outfile = args['outfile']
    input_file = args['data']
    T, eta_s, n_s, theta, lambd = np.loadtxt(args['config'], comments='#')
    theta = np.deg2rad(theta)


    time, data = np.loadtxt(input_file, delimiter=',', unpack=True, skiprows=1)
    data_hat = autocorrelate(data)

    fig, ax = plt.subplots(2,1)
    ax[0].plot(time, data, 'b-')
    ax[0].set_xlabel('Time')
    ax[0].set_ylabel('Voltage Reading')
    ax[1].semilogx(time, data_hat)
    ax[1].set_xlabel('Time')
    ax[1].set_ylabel('Autocorrelation')

    fig.savefig(outfile+'_plot.png')
    
    if args['visualise']:
        plt.show()
    else:
        pass

    q = 4*np.pi/lambd*n_s*np.sin(theta/2)

    tau = 1e-6*time
    ydata = data_hat

    a0 = ydata[-1]
    b0 = ydata[0]- a0
    c0 = 2*q**2*k*T/(18*1e-7*eta_s)

    params, error = curve_fit(g, tau, ydata, p0=[a0,b0,c0])

    (a,b,c) = params

    D = c/(2*q**2)
    R = k*T/(6*np.pi*D*eta_s)
    psize = 2*R*1e9

    fit_err = np.sqrt(np.diag(error))
    dc_over_c = fit_err[2]/c
    psize_err = dc_over_c*psize

    plt.clf()
    plt.semilogx(tau, ydata, 'k.', label='Experiment')
    plt.semilogx(tau, g(tau, a, b, c), 'r-', label='fit: Diffusion = %.2E m^2/s, Particle size=%d nm, ' % (D,psize))
    plt.semilogx(tau, g(tau, a, b, 0.83*c), 'r--', label='+/- 20% in size')
    plt.semilogx(tau, g(tau, a, b, 1.25*c), 'r--')
    plt.xlabel('Tau (s)')
    plt.ylabel('Auto-correlation')
    plt.legend()
    plt.savefig(outfile+'_fit.png')
    
    if args['visualise']:
        plt.show()

    else:
        pass


if __name__ == '__main__':
    main()



    

    
